FROM ubuntu:22.04

LABEL maintainer="cschu1981@gmail.com"
LABEL version="0.1.1"
LABEL description="bowtie2/samtools"


ARG DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y

RUN apt install -y wget python3-pip git dirmngr gnupg ca-certificates build-essential libssl-dev libcurl4-gnutls-dev libxml2-dev libfontconfig1-dev libharfbuzz-dev libfribidi-dev libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev samtools minimap2
RUN apt clean

# RUN sed -i 's:"/tmp":"\$ENV{TMPDIR}":g' /usr/bin/bowtie2


# RUN mkdir -p /opt/software && cd /opt/software

# RUN wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
# bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/software/miniconda3 && \
# rm -f Miniconda3-latest-Linux-x86_64.sh 

# ENV PATH /opt/software/miniconda3/bin:$PATH

  
